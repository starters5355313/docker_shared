# About

The repository serves as a docker support starter for C++ projects.

Build docker image with

```bash
docker build -t docker_shared_image \
	--build-arg UID=`id -u` \
	.
```

command.

Run docker container with

```bash
docker run -it --name docker_shared \
	--detach \
	--network host \
	--volume `realpath .`:/source \
	--workdir /source \
	docker_shared_image
```

command in detached mode.

Join running container with

```bash
docker exec -it docker_shared /bin/bash
```

command.

Build with

```bash
cmake -B build -S .
cmake --build build -j16
```

commands.

Run sample with

```console
$ ./build/hello 
hello!
```

command.
